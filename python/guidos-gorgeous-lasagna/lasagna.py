EXPECTED_BAKE_TIME = 40

def bake_time_remaining(elapsed_bake_time: int):
    """
    Returns the remaining time in the oven
    """
    return EXPECTED_BAKE_TIME - elapsed_bake_time 

def preparation_time_in_minutes(number_of_layers: int):
    """
    Returns how long it took to prep the layers
    """
    return number_of_layers * 2

def elapsed_time_in_minutes(number_of_layers, elapsed_bake_time: int):
    """
    Return elapsed cooking time.

    This function takes two numbers representing the number of layers & the time already spent 
    baking and calculates the total elapsed minutes spent cooking the lasagna.
    """
    return  elapsed_bake_time + preparation_time_in_minutes(number_of_layers)
