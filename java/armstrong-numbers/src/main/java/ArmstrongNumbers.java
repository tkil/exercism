import java.util.ArrayList;
import java.util.List;

class ArmstrongNumbers {

    boolean isArmstrongNumber(int numberToCheck) {
        int[] digits = splitNumberToDigits(numberToCheck);
        int aSum = 0;
        for (int digit: digits) {
            aSum += Math.pow(digit, digits.length);
        }
        return aSum == numberToCheck;
    }

    int[] splitNumberToDigits(int number) {
        int size = 1;
        int remaining = number;
        while (remaining >= 10) {
            size++;
            remaining /= 10;
        }
        int[] digits = new int[size];
        remaining = number;
        for (int i = 0; i < size; i++) {
            int place = (int) (Math.pow(10, size - i - 1));
            digits[i] = (int) (remaining / place);
            remaining -= digits[i] * place;
        }
        return digits;
    }
}
