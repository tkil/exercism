import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.*;

public class ArmstrongNumbersTest {

    private ArmstrongNumbers armstrongNumbers;

    @Before
    public void setup() {
        armstrongNumbers = new ArmstrongNumbers();
    }


    @Test
    public void splitsZero() {
        int[] actual = armstrongNumbers.splitNumberToDigits(0);
        int[] expected = {0};
        assertArrayEquals(expected, actual);
    }


    @Test
    public void splitsTen() {
        int[] actual = armstrongNumbers.splitNumberToDigits(10);
        int[] expected = {1, 0};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void splitsHundred() {
        int[] actual = armstrongNumbers.splitNumberToDigits(100);
        int[] expected = {1, 0, 0};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void splits123() {
        int[] actual = armstrongNumbers.splitNumberToDigits(123);
        int[] expected = {1, 2, 3};
        assertArrayEquals(expected, actual);
    }

    @Test
    public void zeroIsArmstrongNumber() {
        int input = 0;
        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void singleDigitsAreArmstrongNumbers() {
        int input = 5;

        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void noTwoDigitArmstrongNumbers() {
        int input = 10;

        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void threeDigitNumberIsArmstrongNumber() {
        int input = 153;
        
        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void threeDigitNumberIsNotArmstrongNumber() {
        int input = 100;
        
        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void fourDigitNumberIsArmstrongNumber() {
        int input = 9474;
        
        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void fourDigitNumberIsNotArmstrongNumber() {
        int input = 9475;
        
        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void sevenDigitNumberIsArmstrongNumber() {
        int input = 9926315;
        
        assertTrue(armstrongNumbers.isArmstrongNumber(input));
    }

    @Test
    public void sevenDigitNumberIsNotArmstrongNumber() {
        int input = 9926314;
        
        assertFalse(armstrongNumbers.isArmstrongNumber(input));
    }

}
