class Darts {
    private double x;
    private double y;

    Darts(double x, double y) {
        this.x = x;
        this.y = y;
    }

    int score() {
        double distance = this.distance();
        if (distance > 10) {
            return 0;
        }
        if (distance > 5) {
            return 1;
        }
        if (distance > 1) {
            return 5;
        }
        return 10;
    }

    double distance() {
        return Math.sqrt(x * x + y * y);
    }
}
