import java.util.Arrays;

class ResistorColorDuo {
    int value(String[] colors) {
        int val0 = Arrays.asList(getColors()).indexOf(colors[0]);
        int val1 = Arrays.asList(getColors()).indexOf(colors[1]);
        return val0 * 10 + val1;
    }

    String[] getColors() {
        String[] colors = {
            "black",
            "brown",
            "red",
            "orange",
            "yellow",
            "green",
            "blue",
            "violet",
            "grey",
            "white",
        };
        return colors;
    }
}
