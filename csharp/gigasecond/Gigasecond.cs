using System;

public static class Gigasecond
{
    static void Main(string[] args)
    {
        
    }

    public static DateTime Add(DateTime moment)
    {
        const int gs = 1_000_000_000;
        return moment.AddSeconds(gs);
    }
}