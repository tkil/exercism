﻿using System;

public static class HelloWorld
{
    static void Main(string[] args)
    {
        // Display the number of command line arguments.
        Console.WriteLine(args.Length);
    }
    
    public static string Hello()
    {
        return ("Hello, World!");
    }
}
