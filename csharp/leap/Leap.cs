using System;

public static class Leap
{
    static void Main(string[] args)
    {
    }
    
    public static bool IsLeapYear(int year) =>
        year switch
        {
            var y when y % 400 == 0 => true,
            var y when y % 100 == 0 => false,
            var y when y % 4 == 0 => true,
            _ => false
        };
}