export const calcDistanceFromCenter = (x, y) => {
  return Math.sqrt(x * x + y * y);
};

export const score = (x, y) => {
  const distance = calcDistanceFromCenter(x, y);
  switch (true) {
    case distance <= 1:
      return 10;
    case distance <= 5:
      return 5;
    case distance <= 10:
      return 1;
  }
  return 0;
};
