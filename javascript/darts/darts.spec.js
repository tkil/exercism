import { calcDistanceFromCenter, score } from "./darts";

describe("Darts", () => {
  describe("Tyler Tests", () => {
    describe("calcDistanceFromCenter()", () => {
      it("should calc 0 for 0, 0", () => {
        // Act
        const actual = calcDistanceFromCenter(0, 0);
        // Assert
        expect(actual).toEqual(0);
      });
      it("should calc 1 for 1, 0", () => {
        // Act
        const actual = calcDistanceFromCenter(1, 0);
        // Assert
        expect(actual).toEqual(1);
      });
      it("should calc 5 for 3, 4", () => {
        // Act
        const actual = calcDistanceFromCenter(3, 4);
        // Assert
        expect(actual).toEqual(5);
      });
    });
    describe("score()", () => {
      it("should have score of 0 for 11, 11", () => {
        // Act
        const actual = score(11, 11);
        // Assert
        expect(actual).toEqual(0);
      });
      it("should have score of 1 for 0, 10", () => {
        // Act
        const actual = score(0, 10);
        // Assert
        expect(actual).toEqual(1);
      });
      it("should have score of 1 for 7, 7", () => {
        // Act
        const actual = score(7, 7);
        // Assert
        expect(actual).toEqual(1);
      });
      it("should have score of 5 for 0, 5", () => {
        // Act
        const actual = score(0, 5);
        // Assert
        expect(actual).toEqual(5);
      });
      it("should have score of 5 for 3, 4", () => {
        // Act
        const actual = score(3, 4);
        // Assert
        expect(actual).toEqual(5);
      });
      it("should have score of 10 for 0, 1", () => {
        // Act
        const actual = score(0, 1);
        // Assert
        expect(actual).toEqual(10);
      });
      it("should have score of 10 for 0.7, 0.7", () => {
        // Act
        const actual = score(0.7, 0.7);
        // Assert
        expect(actual).toEqual(10);
      });
    });
  });
  describe("Darts", () => {
    test("Missed target", () => {
      expect(score(-9, 9)).toEqual(0);
    });

    test("On the outer circle", () => {
      expect(score(0, 10)).toEqual(1);
    });

    test("On the middle circle", () => {
      expect(score(-5, 0)).toEqual(5);
    });

    test("On the inner circle", () => {
      expect(score(0, -1)).toEqual(10);
    });

    test("Exactly on centre", () => {
      expect(score(0, 0)).toEqual(10);
    });

    test("Near the centre", () => {
      expect(score(-0.1, -0.1)).toEqual(10);
    });

    test("Just within the inner circle", () => {
      expect(score(0.7, 0.7)).toEqual(10);
    });

    test("Just outside the inner circle", () => {
      expect(score(0.8, -0.8)).toEqual(5);
    });

    test("Just within the middle circle", () => {
      expect(score(-3.5, 3.5)).toEqual(5);
    });

    test("Just outside the middle circle", () => {
      expect(score(-3.6, -3.6)).toEqual(1);
    });

    test("Just within the outer circle", () => {
      expect(score(-7.0, 7.0)).toEqual(1);
    });

    test("Just outside the outer circle", () => {
      expect(score(7.1, -7.1)).toEqual(0);
    });

    test("Asymmetric position between the inner and middle circles", () => {
      expect(score(0.5, -4)).toEqual(5);
    });
  });
});
