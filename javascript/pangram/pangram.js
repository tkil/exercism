export const strToUsedMap = (str) => {
  const map = new Map();
  for (const c of str) {
    map.set(c, false);
  }
  return map;
};

export const isPangram = (str) => {
  const used = strToUsedMap("abcdefghijklmnopqrstuvwxyz");
  for (const c of str.toLowerCase()) {
    used.set(c, true);
  }

  for (const [key, isFound] of used) {
    if (!isFound) {
      return false;
    }
  }
  return true;
};
