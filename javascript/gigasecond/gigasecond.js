const GIGA_SECOND = 1000 * 1000 * 1000 * 1000;

export const gigasecond = (date) => {
  return new Date(date.valueOf() + GIGA_SECOND);
};
