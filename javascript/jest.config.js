module.exports = {
  clearMocks: true,
  collectCoverage: false,
  coverageDirectory: "coverage",
  coveragePathIgnorePatterns: ["/cli/", "/node_modules/"],
  moduleFileExtensions: ["js", "jsx", "ts", "tsx", "vue", "json"],
  // setupFilesAfterEnv: ["<rootDir>/jest.setup.ts"],
  moduleNameMapper: {
    "^@/(.*)$": "<rootDir>/src/$1",
    "@env/(.*)": "<rootDir>/env/$1",
    "@modules/(.*)": "<rootDir>/node_modules/$1",
  },
  modulePathIgnorePatterns: [],
  testMatch: ["**/*.(spec|test).(js|jsx|ts|tsx)"],
  testPathIgnorePatterns: ["/node_modules/"],
  transformIgnorePatterns: ["<rootDir>/node_modules/"],
  transform: {
    "^.+\\.jsx?$": "babel-jest",
  },
};
