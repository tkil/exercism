const geneMap = new Map();
geneMap.set("G", "C");
geneMap.set("C", "G");
geneMap.set("T", "A");
geneMap.set("A", "U");

export const toRna = (str) => {
  const result = [];
  for (const c of str) {
    const r = geneMap.get(c);
    result.push(r);
  }
  return result.join("");
};
