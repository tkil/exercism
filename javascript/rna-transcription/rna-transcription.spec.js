import { toRna } from "./rna-transcription";

describe("Test Suite", () => {
  describe("Tyler Tests", () => {
    describe("toRna()", () => {
      it("should return C for G", () => {
        // Act
        const actual = toRna("G");
        // Assert
        expect(actual).toEqual("C");
      });
      it("should return G for C", () => {
        // Act
        const actual = toRna("C");
        // Assert
        expect(actual).toEqual("G");
      });
      it("should return A for T", () => {
        // Act
        const actual = toRna("T");
        // Assert
        expect(actual).toEqual("A");
      });
      it("should return U for A", () => {
        // Act
        const actual = toRna("A");
        // Assert
        expect(actual).toEqual("U");
      });
    });
  });
  describe("Transcription", () => {
    test("empty rna sequence", () => {
      expect(toRna("")).toEqual("");
    });

    test("transcribes cytosine to guanine", () => {
      expect(toRna("C")).toEqual("G");
    });

    test("transcribes guanine to cytosine", () => {
      expect(toRna("G")).toEqual("C");
    });

    test("transcribes thymine to adenine", () => {
      expect(toRna("T")).toEqual("A");
    });

    test("transcribes adenine to uracil", () => {
      expect(toRna("A")).toEqual("U");
    });

    test("transcribes all dna nucleotides to their rna complements", () => {
      expect(toRna("ACGTGGTCTTAA")).toEqual("UGCACCAGAAUU");
    });
  });
});
