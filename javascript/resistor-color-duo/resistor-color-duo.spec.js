import { decodedValue, getValueForColor } from "./resistor-color-duo.js";

describe("Test Suite", () => {
  describe("Tylers Tests", () => {
    describe("getValueForColor()", () => {
      it("should return 0 for black", () => {
        // Act
        const actual = getValueForColor("black");
        // Assert
        expect(actual).toEqual(0);
      });
      it("should return 2 for red", () => {
        // Act
        const actual = getValueForColor("red");
        // Assert
        expect(actual).toEqual(2);
      });
    });
    describe("decodedValue()", () => {
      it("should return 1 for [black, brown]", () => {
        // Act
        const actual = decodedValue(["black", "brown"]);
        // Assert
        expect(actual).toEqual(1);
      });
      it("should return 6 for [red, yellow]", () => {
        // Act
        const actual = decodedValue(["red", "yellow"]);
        // Assert
        expect(actual).toEqual(24);
      });
    });
  });
  describe("Resistor Colors", () => {
    test("Brown and black", () => {
      expect(decodedValue(["brown", "black"])).toEqual(10);
    });

    test("Blue and grey", () => {
      expect(decodedValue(["blue", "grey"])).toEqual(68);
    });

    test("Yellow and violet", () => {
      expect(decodedValue(["yellow", "violet"])).toEqual(47);
    });

    test("Orange and orange", () => {
      expect(decodedValue(["orange", "orange"])).toEqual(33);
    });

    test("Ignore additional colors", () => {
      expect(decodedValue(["green", "brown", "orange"])).toEqual(51);
    });
  });
});
