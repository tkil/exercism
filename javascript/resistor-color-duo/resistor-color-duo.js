export const COLORS = ["black", "brown", "red", "orange", "yellow", "green", "blue", "violet", "grey", "white"];

export const getValueForColor = (color) => {
  return COLORS.indexOf(color);
};

export const decodedValue = (colors) => {
  const band1 = getValueForColor(colors[0]);
  const band2 = getValueForColor(colors[1]);
  return Number(String(band1) + String(band2));
};
