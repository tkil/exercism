(in-package #:cl-user)
(defpackage #:two-fer
  (:use #:cl)
  (:export #:twofer))
(in-package #:two-fer)

(defun twofer (&optional (name "you"))
  "Returns a string sharing one for each"
  (if (not name)
    (setq name "you")
  )
  (format nil "One for ~d, one for me." name)
)
