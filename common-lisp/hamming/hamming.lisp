(defpackage #:hamming
  (:use #:cl)
  (:export #:distance))

(in-package #:hamming)

(defun distance (dna1 dna2)
  "Number of positional differences in two equal length dna strands."
    (let ((count 0)))  
    (loop
      :for i :below (length dna1) :by 1
      :do
        (if (= (length dna1) (length dna2))
          (if (string= (char dna1 i) (char dna2 i))
          (print (char dna1 i))

            ;; (setq count (+ count 1))
          )
        )
      ;; :do (print (length dna2))
    )
)
    
